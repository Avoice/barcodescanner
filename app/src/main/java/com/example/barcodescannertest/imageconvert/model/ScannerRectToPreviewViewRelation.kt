package com.example.barcodescannertest.imageconvert.model

data class ScannerRectToPreviewViewRelation(
    val relativePosX: Float,
    val relativePosY: Float,
    val relativeWidth: Float,
    val relativeHeight: Float
)