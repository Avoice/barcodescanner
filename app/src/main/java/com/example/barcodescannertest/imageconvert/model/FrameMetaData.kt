package com.example.barcodescannertest.imageconvert.model

data class FrameMetadata(val width: Int, val height: Int, val rotation: Int)