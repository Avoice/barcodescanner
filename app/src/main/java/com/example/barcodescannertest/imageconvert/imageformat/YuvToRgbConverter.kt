package com.example.barcodescannertest.imageconvert.imageformat

import android.graphics.Bitmap
import android.graphics.Rect
import android.media.Image

interface YuvToRgbConverter {
    /**
     * Make sure to add try/catch around the place where you call this method as it may throw some
     * exceptions due to unpredictable image conversion issues on different devices
     */
    fun convert(image: Image, rotation: Int, cropRect: Rect? = null): Bitmap
}