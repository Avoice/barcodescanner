package com.example.barcodescannertest.imageconvert.utils

import android.graphics.Rect
import android.media.Image
import android.util.Size
import com.example.barcodescannertest.analyzing.model.ScannerMeasurements
import com.example.barcodescannertest.imageconvert.model.ScannerRectToPreviewViewRelation

fun getCropRectAccordingToRotation(
    image: Image,
    relationRect: ScannerRectToPreviewViewRelation,
    rotation: Int
): Rect {
    return when (rotation) {
        0 -> {
            val startX = (relationRect.relativePosX * image.width).toInt()
            val numberPixelW = (relationRect.relativeWidth * image.width).toInt()
            val startY = (relationRect.relativePosY * image.height).toInt()
            val numberPixelH = (relationRect.relativeHeight * image.height).toInt()
            Rect(startX, startY, startX + numberPixelW, startY + numberPixelH)
        }
        90 -> {
            val startX = (relationRect.relativePosY * image.width).toInt()
            val numberPixelW = (relationRect.relativeHeight * image.width).toInt()
            val startY = (relationRect.relativePosX * image.height).toInt()
            val numberPixelH = (relationRect.relativeWidth * image.height).toInt()
            Rect(startX, startY, startX + numberPixelW, startY + numberPixelH)
        }
        180 -> {
            val numberPixelW = (relationRect.relativeWidth * image.width).toInt()
            val startX =
                (image.width - relationRect.relativePosX * image.width - numberPixelW).toInt()
            val numberPixelH = (relationRect.relativeHeight * image.height).toInt()
            val startY =
                (image.height - relationRect.relativePosY * image.height - numberPixelH).toInt()
            Rect(startX, startY, startX + numberPixelW, startY + numberPixelH)
        }
        270 -> {
            val numberPixelW = (relationRect.relativeHeight * image.width).toInt()
            val numberPixelH = (relationRect.relativeWidth * image.height).toInt()
            val startX =
                (image.width - relationRect.relativePosY * image.width - numberPixelW).toInt()
            val startY = (relationRect.relativePosX * image.height).toInt()
            Rect(startX, startY, startX + numberPixelW, startY + numberPixelH)
        }
        else -> throw IllegalArgumentException("Rotation degree ($rotation) not supported!")
    }
}

fun getScannerRectToPreviewViewRelation(
    scannerMeasurements: ScannerMeasurements,
    proxySize: Size,
    rotation: Int
): ScannerRectToPreviewViewRelation {
    // This calculations are based on the assumption that the scale type
    // of the PreviewView is FILL CENTER. If it's different - the calculation will be wrong
    // Delta (width or height) - is the difference between real preview height/width and the one
    // user sees. Since we assume that the center of the real and the visible are in the same dot
    // we can calculate startX/Y by adding half of the delta to left/top X/Y coordinate
    val scannerWindowRect = scannerMeasurements.scannerWindowInPreviewRelativeCoordinates
    val visiblePreviewWidth = scannerMeasurements.previewViewSize.width
    val visiblePreviewHeight = scannerMeasurements.previewViewSize.height

    val realPreviewWidth: Float
    val realPreviewHeight: Float

    when (rotation) {
        0, 180 -> {
            if (visiblePreviewWidth > visiblePreviewHeight && proxySize.width > proxySize.height) {
                realPreviewWidth = visiblePreviewWidth.toFloat()
                realPreviewHeight = realPreviewWidth / proxySize.widthToHeightRatio
            } else {
                realPreviewWidth = visiblePreviewHeight * proxySize.widthToHeightRatio
                realPreviewHeight = visiblePreviewHeight.toFloat()
            }
        }
        90, 270 -> {
            if (visiblePreviewWidth > visiblePreviewHeight && proxySize.width > proxySize.height) {
                realPreviewWidth = visiblePreviewWidth.toFloat()
                realPreviewHeight = visiblePreviewWidth * proxySize.widthToHeightRatio
            } else {
                realPreviewWidth = visiblePreviewHeight / proxySize.widthToHeightRatio
                realPreviewHeight = visiblePreviewHeight.toFloat()
            }

        }
        else -> throw IllegalArgumentException("Rotation degree ($rotation) not supported!")
    }

    val heightDeltaTop = (realPreviewHeight - visiblePreviewHeight) / 2
    val widthDeltaLeft = (realPreviewWidth - visiblePreviewWidth) / 2

    val rectStartX = widthDeltaLeft + scannerWindowRect.left
    val rectStartY = heightDeltaTop + scannerWindowRect.top

    return ScannerRectToPreviewViewRelation(
        rectStartX / realPreviewWidth,
        rectStartY / realPreviewHeight,
        scannerWindowRect.width() / realPreviewWidth,
        scannerWindowRect.height() / realPreviewHeight
    )
}

private val Size.widthToHeightRatio get() = width.toFloat() / height
