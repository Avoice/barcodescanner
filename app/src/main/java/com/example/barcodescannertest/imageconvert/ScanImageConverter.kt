package com.example.barcodescannertest.imageconvert

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Rect
import android.media.Image
import android.util.Log
import android.util.Size
import androidx.camera.core.ImageProxy
import com.example.barcodescannertest.analyzing.model.ScanReadyImage
import com.example.barcodescannertest.analyzing.model.ScannerMeasurements
import com.example.barcodescannertest.imageconvert.imageformat.YuvToRgbOnFlyCropConverter
import com.example.barcodescannertest.imageconvert.imageformat.YuvToRgbRenderScriptConverter
import com.example.barcodescannertest.imageconvert.utils.BitmapUtils
import com.example.barcodescannertest.imageconvert.utils.getCropRectAccordingToRotation
import com.example.barcodescannertest.imageconvert.utils.getScannerRectToPreviewViewRelation
import com.google.mlkit.vision.common.InputImage

class ScanImageConverter(
    private val imageRenderScriptConverter: YuvToRgbRenderScriptConverter,
    private val imageOnFlyCropConverter: YuvToRgbOnFlyCropConverter
) {

    private lateinit var bitmapBuffer: Bitmap

    @SuppressLint("UnsafeExperimentalUsageError")
    fun convert(
        imageProxy: ImageProxy,
        scannerMeasurements: ScannerMeasurements
    ): ScanReadyImage? {
        val mediaImage = imageProxy.image ?: return null
        val rotation = imageProxy.imageInfo.rotationDegrees
        val size = Size(imageProxy.width, imageProxy.height)
        val scannerRect =
            getScannerRectToPreviewViewRelation(
                scannerMeasurements,
                size,
                rotation
            )

        val cropRect =
            getCropRectAccordingToRotation(
                mediaImage,
                scannerRect,
                rotation
            )

        val bitmap = getBitmap(mediaImage, rotation, cropRect)
        val inputImage = bitmap?.let { InputImage.fromBitmap(bitmap, 0) }
            ?: InputImage.fromMediaImage(mediaImage, rotation)

        return ScanReadyImage(
            inputImage,
            bitmap
        )
    }

    private fun getBitmap(mediaImage: Image, rotation: Int, cropRect: Rect): Bitmap? = try {
        getBitmapMethod0(mediaImage, rotation, cropRect)
    } catch (e: Exception) {
        Log.e("ScanImageConverter", "Error while convering yuv to rgb", e)
        getBitmap(mediaImage, rotation, cropRect, 1)
        null
    }

    private fun getBitmap(mediaImage: Image, rotation: Int, cropRect: Rect, tryCount: Int): Bitmap? = try {
        when (tryCount) {
            0 -> getBitmapMethod0(mediaImage, rotation, cropRect)
            1 -> getBitmapMethod1(mediaImage, rotation, cropRect)
            2 -> getBitmapMethod2(mediaImage, rotation, cropRect)
            else -> null
        }
    } catch (e: Exception) {
        Log.e("ScanImageConverter", "Error while convering yuv to rgb", e)
        getBitmap(mediaImage, rotation, cropRect, tryCount + 1)
    }

    // Uses Google's conversion with the RenderScript. But Google left a note, that the behaviour
    // might be inconsistent across different devices.
    // Also cropping-while-converting yuv to rgb doesn't work, so we convert whole image to rgb
    // and then crop it
    private fun getBitmapMethod0(
        mediaImage: Image,
        rotation: Int,
        cropRect: Rect
    ): Bitmap {
        if (!::bitmapBuffer.isInitialized) {
            bitmapBuffer = Bitmap.createBitmap(
                mediaImage.width, mediaImage.height, Bitmap.Config.ARGB_8888
            )
        }

        return imageRenderScriptConverter.convert(bitmapBuffer, mediaImage, rotation, cropRect)
    }

    // Uses the same conversion as the [getBitmapMethod2], but converts whole image to rgb and then
    // crop resulted rgb image. More laggy than [getBitmapMethod0] and [getBitmapMethod2]
    private fun getBitmapMethod1(
        mediaImage: Image,
        rotation: Int,
        cropRect: Rect
    ): Bitmap {
        val unCroppedNotRotatedBitmap = imageOnFlyCropConverter.convert(mediaImage, 0)
        return BitmapUtils.rotateAndCrop(
            unCroppedNotRotatedBitmap,
            rotation,
            cropRect
        )
    }

    // Crops image while converting yuv to rgb which reduces conversion time,
    // but some times it produces green stripes on the image in some width/height ratio cases
    // which can affect barcode detection
    private fun getBitmapMethod2(
        mediaImage: Image,
        rotation: Int,
        cropRect: Rect
    ): Bitmap = imageOnFlyCropConverter.convert(mediaImage, rotation, cropRect)

}