package com.example.barcodescannertest.imageconvert.imageformat

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageFormat
import android.graphics.Rect
import android.graphics.YuvImage
import android.media.Image
import com.example.barcodescannertest.imageconvert.model.FrameMetadata
import com.example.barcodescannertest.imageconvert.utils.BitmapUtils
import java.io.ByteArrayOutputStream

class YuvToRgbOnFlyCropConverter :
    YuvToRgbConverter {

    override fun convert(image: Image, rotation: Int, cropRect: Rect?): Bitmap {
        image.cropRect = cropRect
        val metadata = cropRect
            ?.let {
                FrameMetadata(
                    cropRect.width(),
                    cropRect.height(),
                    rotation
                )
            }
            ?: FrameMetadata(
                image.width,
                image.height,
                rotation
            )
        val byteArray = yuv420toNV21(image)
        return getBitmap(byteArray, metadata)
    }

    fun yuv420toNV21(image: Image): ByteArray {
        val crop = image.cropRect
        val format = image.format
        val width = crop.width()
        val height = crop.height()

        val planes = image.planes
        val pixelCount = width * height
        val data = ByteArray(pixelCount * ImageFormat.getBitsPerPixel(format) / 8)
        val rowData = ByteArray(planes[0].rowStride)
        var channelOffset = 0
        var outputStride = 1
        for (i in planes.indices) {
            when (i) {
                0 -> {
                    channelOffset = 0
                    outputStride = 1
                }
                1 -> {
                    channelOffset = pixelCount + 1
                    outputStride = 2
                }
                2 -> {
                    channelOffset = pixelCount
                    outputStride = 2
                }
            }
            val buffer = planes[i].buffer
            val rowStride = planes[i].rowStride
            val pixelStride = planes[i].pixelStride
            val shift = if (i == 0) 0 else 1
            val w = width shr shift
            val h = height shr shift
            buffer.position(rowStride * (crop.top shr shift) + pixelStride * (crop.left shr shift))
            for (row in 0 until h) {
                var length: Int
                if (pixelStride == 1 && outputStride == 1) {
                    length = w
                    buffer[data, channelOffset, length]
                    channelOffset += length
                } else {
                    length = (w - 1) * pixelStride + 1
                    buffer[rowData, 0, length]
                    for (col in 0 until w) {
                        data[channelOffset] = rowData[col * pixelStride]
                        channelOffset += outputStride
                    }
                }
                if (row < h - 1) {
                    buffer.position(buffer.position() + rowStride - length)
                }
            }
        }
        return data
    }

    private fun getBitmap(data: ByteArray, metadata: FrameMetadata): Bitmap {

        val image = YuvImage(
            data, ImageFormat.NV21, metadata.width, metadata.height, null
        )
        val stream = ByteArrayOutputStream()
        image.compressToJpeg(
            Rect(0, 0, metadata.width, metadata.height),
            80,
            stream
        )
        val bitmap = BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.size())
        stream.close()
        return BitmapUtils.rotateBitmap(bitmap, metadata.rotation)
    }
}