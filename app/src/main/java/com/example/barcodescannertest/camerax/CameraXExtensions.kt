package com.example.barcodescannertest.camerax

import android.content.Context
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import kotlinx.coroutines.suspendCancellableCoroutine
import java.util.concurrent.CancellationException
import java.util.concurrent.ExecutionException
import kotlin.coroutines.resume

suspend fun Context.getCameraProvider(): ProcessCameraProvider =
    suspendCancellableCoroutine { continuation ->

        val instanceFuture = ProcessCameraProvider.getInstance(this)

        continuation.invokeOnCancellation { instanceFuture.cancel(true) }

        val mainExecutor = ContextCompat.getMainExecutor(this)
        instanceFuture.addListener(
            Runnable {
                if (continuation.isCancelled) return@Runnable
                val processCameraProvider = try {
                    instanceFuture.get()
                } catch (e: ExecutionException) {
                    throw IllegalStateException("Camera initialization failed.", e.cause)
                } catch (e: InterruptedException) {
                    throw IllegalStateException("Camera initialization failed.", e.cause)
                } catch (e: CancellationException) {
                    throw IllegalStateException("Camera initialization failed.", e.cause)
                }
                continuation.resume(processCameraProvider)
            },
            mainExecutor
        )
    }