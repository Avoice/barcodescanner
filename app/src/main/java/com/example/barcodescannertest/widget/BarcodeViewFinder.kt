package com.example.barcodescannertest.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.CornerPathEffect
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import androidx.core.content.res.ResourcesCompat
import com.example.barcodescannertest.R
import com.example.barcodescannertest.uiutils.dp

private const val ANIMATION_DELAY = 100L

class BarcodeViewFinder @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) :
    View(context, attrs), ScannerOverlay {

    private val scannerAlphaSteps = intArrayOf(0, 64, 128, 192, 255, 192, 128, 64)

    private var currentScannerAlpha: Int = 0

    private val aimStrokeWidth =
        context.resources.getDimensionPixelSize(R.dimen.view_finder_barcket_stroke_width)
    private val aimBracketLength =
        context.resources.getDimensionPixelSize(R.dimen.view_finder_bracket_length)

    private val aimPaint = Paint().apply {
        pathEffect = CornerPathEffect(8f.dp(context))
        style = Paint.Style.STROKE
        strokeCap = Paint.Cap.ROUND
        strokeJoin = Paint.Join.ROUND
        strokeWidth = aimStrokeWidth.toFloat()
        color = Color.WHITE
        isAntiAlias = true
    }

    private val laserWidth =
        context.resources.getDimensionPixelSize(R.dimen.view_finder_barcket_stroke_width) * 1.5f

    private val laserPaint = Paint().apply {
        color = ResourcesCompat.getColor(context.resources, (R.color.barcodeScannerLaser), null)
        style = Paint.Style.STROKE
        strokeCap = Paint.Cap.ROUND
        strokeWidth = laserWidth.toFloat()
    }

    private val scannerWindowPath: Path = Path()

    private val laserHorizontalMargin = 10f.dp(context)
    private val laserPath: Path = Path()

    override val scannerWindowRectInLocalCoordinates: RectF = RectF()

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        updateScannerWindowRect()
        updateScannerWindowPath()
        updateLaserPath()
    }

    private fun updateScannerWindowPath() {
        val padding = aimStrokeWidth / 2f
        scannerWindowPath.apply {
            reset()

            // top left
            moveTo(padding, padding + aimBracketLength)
            lineTo(padding, padding)
            lineTo(padding + aimBracketLength, padding)

            // top right
            val scanRectRight = scannerWindowRectInLocalCoordinates.right
            val scanRectBottom = scannerWindowRectInLocalCoordinates.bottom
            moveTo(scanRectRight - padding, padding + aimBracketLength)
            lineTo(scanRectRight - padding, padding)
            lineTo(scanRectRight - padding - aimBracketLength, padding)

            // bottom right
            moveTo(scanRectRight - padding, scanRectBottom - padding - aimBracketLength)
            lineTo(scanRectRight - padding, scanRectBottom - padding)
            lineTo(scanRectRight - padding - aimBracketLength, scanRectBottom - padding)

            // bottom left
            moveTo(padding, scanRectBottom - padding - aimBracketLength)
            lineTo(padding, scanRectBottom - padding)
            lineTo(padding + aimBracketLength, scanRectBottom - padding)
        }
    }

    private fun updateLaserPath() {
        laserPath.apply {
            reset()
            moveTo(laserHorizontalMargin, height / 2f)
            lineTo(width.toFloat() - laserHorizontalMargin, height / 2f)
        }
    }

    private fun updateScannerWindowRect() {
        scannerWindowRectInLocalCoordinates.set(
            0f,
            0f,
            width.toFloat(),
            height.toFloat()
        )
    }

    override fun onDraw(canvas: Canvas?) {
        canvas ?: return
        drawScannerWindow(canvas)
        drawLaser(canvas)
    }

    private fun drawScannerWindow(canvas: Canvas) {
        canvas.drawPath(scannerWindowPath, aimPaint)
    }

    private fun drawLaser(canvas: Canvas) {
        laserPaint.alpha = scannerAlphaSteps[currentScannerAlpha]
        currentScannerAlpha = (currentScannerAlpha + 1) % scannerAlphaSteps.size

        canvas.drawPath(laserPath, laserPaint)

        postInvalidateDelayed(
            ANIMATION_DELAY,
            left,
            top,
            right,
            bottom
        )
    }
}