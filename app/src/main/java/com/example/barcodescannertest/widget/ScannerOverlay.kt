package com.example.barcodescannertest.widget

import android.graphics.RectF

interface ScannerOverlay {

    val scannerWindowRectInLocalCoordinates : RectF
}