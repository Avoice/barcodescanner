package com.example.barcodescannertest.widget

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.camera.core.CameraSelector
import androidx.camera.core.Preview
import androidx.camera.view.PreviewView
import androidx.core.view.isVisible

class PausablePreviewView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    var scaleType: PreviewView.ScaleType = PreviewView.ScaleType.FILL_CENTER

    private val pausedImageView = AppCompatImageView(context).apply {
        layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT
        )
    }

    val previewView: PreviewView = PreviewView(context).apply {
        layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT
        )
    }

    var isPaused: Boolean = false
        private set

    init {
        pausedImageView.isVisible = false
        addView(previewView)
        addView(pausedImageView)
    }

    fun pause() {
        try {
            pausedImageView.setImageBitmap(previewView.bitmap)
            pausedImageView.isVisible = true
            isPaused = true
        } catch (e: Exception) {
            Log.e("PausablePreviewView", "Failed to pause", e)
        }
    }

    fun resume() {
        pausedImageView.isVisible = false
        isPaused = false
    }

    fun createSurfaceProvider(): Preview.SurfaceProvider = previewView.createSurfaceProvider()

    fun createMeteringPointFactory(cameraSelector: CameraSelector) = previewView.createMeteringPointFactory(cameraSelector)
}