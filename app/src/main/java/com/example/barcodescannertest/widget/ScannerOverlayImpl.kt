package com.example.barcodescannertest.widget

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.graphics.RectF
import android.os.Build
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.View
import com.example.barcodescannertest.R

class ScannerOverlayImpl @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr),
    ScannerOverlay {

    private val padding =
        context.resources.getDimensionPixelSize(R.dimen.view_finder_padding)

    private val transparentPaint: Paint by lazy {
        Paint().apply {
            isAntiAlias = true
            xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
        }
    }

    private val strokePaint: Paint by lazy {
        Paint().apply {
            isAntiAlias = true
            color = Color.WHITE
            strokeWidth = context.px(3f)
            style = Paint.Style.STROKE
        }
    }

    override val scannerWindowRectInLocalCoordinates: RectF
        get() {
            val viewFinderWidth = width / 1.5f
            val viewFinderHeight = context.px(120f)
            val parentView = parent as View

            val left = parentView.left + (width - viewFinderWidth - 2 * padding) / 2
            val top = parentView.top + padding.toFloat()
            val right = width - left
            val bottom = top + viewFinderHeight
            return RectF(left, top, right, bottom)
        }

    private val drawnRect: RectF
        get() {
            // val viewFinderWidth = width / 1.5f
            // val viewFinderHeight = context.px(120f)
            //
            // val left = (width - viewFinderWidth - 2 * padding) / 2
            // val top = padding.toFloat()
            // val right = width - left
            // val bottom = top + viewFinderHeight
            // return RectF(left, top, right, bottom)
            return scannerWindowRectInLocalCoordinates
        }

    init {
        setWillNotDraw(false)

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            setLayerType(LAYER_TYPE_HARDWARE, null)
        }
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        canvas.drawColor(Color.parseColor("#88000000"))

        val radius = context.px(4f)
        val rectF = drawnRect
        canvas.drawRoundRect(rectF, radius, radius, transparentPaint)
        strokePaint.color = Color.WHITE
        canvas.drawRoundRect(rectF, radius, radius, strokePaint)
    }

    // override val scanRect: RectF
    //     get() {
    //         val size = min(width * 0.6f, MAX_WIDTH_PORTRAIT)
    //         val l = (width - size) / 2
    //         val r = width - l
    //         val t = height * 0.15f
    //         val b = t + size
    //         return RectF(l, t, r, b)
    //     }

    companion object {
        const val MAX_WIDTH_PORTRAIT = 1200f
        const val MAX_WIDTH_LANDSCAPE = 2000f
    }
}

fun Context.px(densityPixel: Float) =
    calc(this, densityPixel)

private fun calc(context: Context, dp: Float): Float {
    return dp * (context.resources.displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT)
}

fun Context.isPortrait(): Boolean {
    val orientation = this.resources.configuration.orientation
    return orientation == Configuration.ORIENTATION_PORTRAIT
}
