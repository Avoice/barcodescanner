package com.example.barcodescannertest.uiutils

import android.view.View
import androidx.core.view.doOnLayout
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

suspend fun View.waitTillLaidOut(): View = suspendCoroutine { continuation ->
    doOnLayout { continuation.resume(it) }
}