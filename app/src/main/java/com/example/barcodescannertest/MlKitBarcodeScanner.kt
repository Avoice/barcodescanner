package com.example.barcodescannertest

import android.content.Context
import android.graphics.Bitmap
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import androidx.camera.core.Camera
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import com.example.barcodescannertest.analyzing.ScannerImageAnalyzer
import com.example.barcodescannertest.widget.PausablePreviewView
import java.io.IOException

private const val BEEP_VOLUME = 0.10f
private const val VIBRATION_LENGTH_MS = 100L

class MlKitBarcodeScanner(
    private val analyzer: ScannerImageAnalyzer,
    private val previewView: PausablePreviewView,
    private val camera: Camera
) : BarcodeScanner {

    private val context: Context
        get() = previewView.context

    override val scannedBarcodes = analyzer.barcodes
    override var isVibrationEnabled: Boolean = true
    override var isSoundEffectEnabled: Boolean = true

    val scannerFrames: LiveData<Bitmap> = analyzer.frame

    private val vibrator by lazy {
        ContextCompat.getSystemService(context, Vibrator::class.java)
    }

    override fun pause() {
        analyzer.pause()
        previewView.pause()
    }

    override fun resume() {
        analyzer.resume()
        previewView.resume()
    }

    override fun torchOn() {
        if (camera.cameraInfo.hasFlashUnit()) {
            camera.cameraControl.enableTorch(true)
        }
    }

    override fun torchOff() {
        if (camera.cameraInfo.hasFlashUnit()) {
            camera.cameraControl.enableTorch(false)
        }
    }

    override fun playScanEffect() {
        if (isVibrationEnabled) {
            vibrate()
        }

        if (isSoundEffectEnabled) {
            playSoundEffect()
        }
    }

    private fun vibrate() {
        if (Build.VERSION.SDK_INT >= 26) {
            val vibrationEffect = VibrationEffect.createOneShot(
                VIBRATION_LENGTH_MS,
                VibrationEffect.DEFAULT_AMPLITUDE
            )
            vibrator?.vibrate(vibrationEffect)
        } else {
            vibrator?.vibrate(VIBRATION_LENGTH_MS)
        }
    }

    private fun playSoundEffect() {
        val mediaPlayer = createMediaPlayer()
        return try {
            context.resources.openRawResourceFd(R.raw.barcode_scanner_beep).use { file ->
                mediaPlayer.setDataSource(
                    file.fileDescriptor,
                    file.startOffset,
                    file.length
                )
            }
            mediaPlayer.prepare()
            mediaPlayer.start()
        } catch (ioException: IOException) {
            Log.e("MLKitBarcodeScanner", "Error while playing sound effect", ioException)
            mediaPlayer.release()
        }
    }

    private fun createMediaPlayer(): MediaPlayer {
        val mediaPlayer = MediaPlayer()
        mediaPlayer.apply {
            val attributes = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build()

            setAudioAttributes(attributes)

            setOnCompletionListener { player ->
                player.stop()
                player.release()
            }
            setOnErrorListener { player, what, extra ->
                Log.e("MLKitBarcodeScanner","Failed to beep $what, $extra")
                player.stop()
                player.release()
                true
            }
            setVolume(BEEP_VOLUME, BEEP_VOLUME)
        }
        return mediaPlayer
    }
}