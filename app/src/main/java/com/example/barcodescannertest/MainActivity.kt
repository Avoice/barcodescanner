package com.example.barcodescannertest

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.Camera
import androidx.camera.core.CameraSelector
import androidx.camera.core.FocusMeteringAction
import androidx.core.app.ActivityCompat
import androidx.core.view.doOnLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val isCameraPermissionGranted: Boolean
        get() = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
            PackageManager.PERMISSION_GRANTED

    private var isScannerPaused: Boolean = false

    private var barcodeScanner: BarcodeScanner? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val cameraPermissionRequestLauncher = cameraPermissionLauncher()

        if (isCameraPermissionGranted) {
            startCamera()
        } else {
            cameraPermissionRequestLauncher.launch(Manifest.permission.CAMERA)
            return
        }
    }

    private fun cameraPermissionLauncher(): ActivityResultLauncher<String> {
        return registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                startCamera()
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun startCamera() {
        lifecycleScope.launchWhenResumed {
            barcodeScanner = BarcodeScannerProvider.initAndGetMlKitScanner(
                this@MainActivity,
                viewFinder,
                previewView,
                this@MainActivity
            )
            if (barcodeScanner == null) {
                Log.e("MainActivity", "Failed to initialize barcode scanner")
            }
            barcodeScanner?.let {
                it.isSoundEffectEnabled = false
                attachScanner(it)
            }
            setUpButtons()
        }
    }

    private fun attachScanner(barcodeScanner: BarcodeScanner) {
        barcodeScanner.observeBarcodes(this) { barcodes ->
            analyzedValue.text = barcodes.joinToString(separator = "\n")
            barcodeScanner.playScanEffect()
        }

        (barcodeScanner as MlKitBarcodeScanner).scannerFrames.observe(this, Observer {
            viewFinderMirror.setImageBitmap(it)
        })

        barcodeScanner.resume()
    }

    private fun setUpButtons() {
        pauseButton.setOnClickListener {
            isScannerPaused = if (isScannerPaused) {
                barcodeScanner?.resume()
                false
            } else {
                barcodeScanner?.pause()
                true
            }
        }

        torchOnButton.setOnClickListener {
            barcodeScanner?.torchOn()
        }

        torchOffButton.setOnClickListener {
            barcodeScanner?.torchOff()
        }
    }

    private fun setAutoFocusArea(
        camera: Camera,
        cameraSelector: CameraSelector
    ) {
        viewFinder.doOnLayout {

            val cameraControl = camera.cameraControl

            val x = viewFinder.left + viewFinder.scannerWindowRectInLocalCoordinates.centerX()
            val y = viewFinder.top + viewFinder.scannerWindowRectInLocalCoordinates.centerY()

            val meteringPoint = previewView.createMeteringPointFactory(cameraSelector)
                .createPoint(x, y)

            val action = FocusMeteringAction.Builder(meteringPoint).build()
            cameraControl.startFocusAndMetering(action)
            // Handler().postDelayed(
            //     { setAutoFocusArea(camera, cameraSelector) },
            //     2000
            // )
        }
    }
}
