package com.example.barcodescannertest

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

interface BarcodeScanner {

    val scannedBarcodes: LiveData<List<String>>
    var isVibrationEnabled: Boolean
    var isSoundEffectEnabled: Boolean

    fun observeBarcodes(lifecycleOwner: LifecycleOwner, onChanged: (List<String>) -> Unit) {
        scannedBarcodes.observe(lifecycleOwner, Observer(onChanged))
    }

    fun pause()

    fun resume()

    fun torchOn()

    fun torchOff()

    fun playScanEffect()
}
