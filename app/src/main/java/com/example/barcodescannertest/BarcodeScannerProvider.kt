package com.example.barcodescannertest

import android.content.Context
import android.graphics.RectF
import android.util.Log
import android.util.Size
import android.view.View
import androidx.camera.core.Camera
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.view.PreviewView
import androidx.lifecycle.LifecycleOwner
import com.example.barcodescannertest.analyzing.ScannerImageAnalyzer
import com.example.barcodescannertest.analyzing.model.ScannerMeasurements
import com.example.barcodescannertest.camerax.getCameraProvider
import com.example.barcodescannertest.imageconvert.ScanImageConverter
import com.example.barcodescannertest.imageconvert.imageformat.YuvToRgbOnFlyCropConverter
import com.example.barcodescannertest.imageconvert.imageformat.YuvToRgbRenderScriptConverter
import com.example.barcodescannertest.uiutils.waitTillLaidOut
import com.example.barcodescannertest.widget.PausablePreviewView
import com.example.barcodescannertest.widget.ScannerOverlay
import kotlinx.coroutines.withTimeoutOrNull
import java.util.concurrent.Executors

private const val TARGET_PREVIEW_WIDTH = 960
private const val TARGET_PREVIEW_HEIGHT = 1280
private const val PREVIEW_LAY_OUT_MAX_WAITING_MS = 2_000L

object BarcodeScannerProvider {

    suspend fun <T> initAndGetMlKitScanner(
        context: Context,
        viewFinder: T,
        previewView: PausablePreviewView,
        lifecycleOwner: LifecycleOwner
    ): BarcodeScanner? where T : View, T : ScannerOverlay {
        val viewSizesValid = checkViewSizesValidity(viewFinder, previewView)
        if (!viewSizesValid) {
            Log.e("BarcodeScannerProvider","Can't initialize barcode scanner: ViewFinder size ${viewFinder.scannerWindowRectInLocalCoordinates} or Preview size (${previewView.width}x${previewView.height}) was invalid")
            return null
        }

        val cameraProvider = context.getCameraProvider()
        previewView.scaleType = PreviewView.ScaleType.FILL_CENTER // Crucial to have this set to FILL_CENTER for calculating the scanner window crop rectangle to work correctly
        val preview: Preview = createPreviewUseCase(previewView)
        val analyzer = createAnalyzer(context, viewFinder, previewView)
        val imageAnalysis = createAnalysisUseCase(analyzer)
        val cameraSelector: CameraSelector = createCameraSelector()

        val camera: Camera = cameraProvider.bindToLifecycle(
            lifecycleOwner,
            cameraSelector,
            preview,
            imageAnalysis
        )

        return MlKitBarcodeScanner(analyzer, previewView, camera)
    }

    private suspend fun <T> checkViewSizesValidity(
        viewFinder: T,
        previewView: PausablePreviewView
    ): Boolean where T : View, T : ScannerOverlay {
        val scannerWindowRectInLocalCoordinates = viewFinder.scannerWindowRectInLocalCoordinates
        var viewSizesValid = areViewSizesValid(scannerWindowRectInLocalCoordinates, previewView)
        if (!viewSizesValid) {
            return true
        }

        val previewLaidOut = withTimeoutOrNull(PREVIEW_LAY_OUT_MAX_WAITING_MS) { previewView.waitTillLaidOut() } != null
        viewSizesValid = areViewSizesValid(scannerWindowRectInLocalCoordinates, previewView)

        return previewLaidOut && viewSizesValid
    }

    private fun areViewSizesValid(
        scannerWindowRectInLocalCoordinates: RectF,
        previewView: PausablePreviewView
    ): Boolean = scannerWindowRectInLocalCoordinates.width() > 0 &&
        scannerWindowRectInLocalCoordinates.height() > 0 &&
        previewView.width > 0 &&
        previewView.height > 0

    private fun createCameraSelector(): CameraSelector {
        return CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
            .build()
    }

    private fun createAnalysisUseCase(analyzer: ScannerImageAnalyzer): ImageAnalysis {
        return ImageAnalysis.Builder()
            .setTargetResolution(Size(TARGET_PREVIEW_WIDTH, TARGET_PREVIEW_HEIGHT))
            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
            .build()
            .apply { setAnalyzer(Executors.newSingleThreadExecutor(), analyzer) }
    }

    private fun <T> createAnalyzer(
        context: Context,
        viewFinder: T,
        previewView: PausablePreviewView
    ): ScannerImageAnalyzer where T : View, T : ScannerOverlay {
        val imageConverter = ScanImageConverter(
            YuvToRgbRenderScriptConverter(context),
            YuvToRgbOnFlyCropConverter()
        )
        val scannerMeasurements = createScannerMeasurements(viewFinder, previewView)
        return ScannerImageAnalyzer(scannerMeasurements, imageConverter)
    }

    private fun createPreviewUseCase(previewView: PausablePreviewView): Preview {
        return Preview.Builder()
            .setTargetResolution(Size(TARGET_PREVIEW_WIDTH, TARGET_PREVIEW_HEIGHT))
            .build()
            .apply { setSurfaceProvider(previewView.createSurfaceProvider()) }
    }

    private fun <T> createScannerMeasurements(
        viewFinder: T,
        previewView: PausablePreviewView
    ): ScannerMeasurements where T : View, T : ScannerOverlay {
        val left =
            viewFinder.left - previewView.left + viewFinder.scannerWindowRectInLocalCoordinates.left

        val top =
             viewFinder.top - previewView.top + viewFinder.scannerWindowRectInLocalCoordinates.top

        val scanRect = RectF(
            left,
            top,
            left + viewFinder.scannerWindowRectInLocalCoordinates.width(),
            top + viewFinder.scannerWindowRectInLocalCoordinates.height()
        )
        val previewSize = Size(previewView.width, previewView.height)

        if (scanRect.right > previewView.right || scanRect.bottom > previewView.bottom) {
            throw IllegalStateException("Scanner window should be within preview view. Preview: (${previewView.left}, ${previewView.top}, ${previewView.right}, ${previewView.bottom}. Scanner window: ($scanRect)")
        }

        return ScannerMeasurements(previewSize, scanRect)
    }
}