package com.example.barcodescannertest.analyzing.model

import android.graphics.RectF
import android.util.Size

data class ScannerMeasurements (
    val previewViewSize : Size,
    val scannerWindowInPreviewRelativeCoordinates : RectF
)