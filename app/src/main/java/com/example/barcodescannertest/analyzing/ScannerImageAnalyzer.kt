package com.example.barcodescannertest.analyzing

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.media.MediaActionSound
import android.util.Log
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.barcodescannertest.analyzing.model.ScannerMeasurements
import com.example.barcodescannertest.imageconvert.ScanImageConverter
import com.google.android.gms.tasks.Tasks
import com.google.mlkit.vision.barcode.Barcode
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import java.lang.Exception

class ScannerImageAnalyzer(
    private val scannerMeasurements: ScannerMeasurements,
    private val scanImageConverter: ScanImageConverter
) : ImageAnalysis.Analyzer {

    private val mediaActionSound by lazy { MediaActionSound() }

    private val options = BarcodeScannerOptions.Builder()
        .setBarcodeFormats(
            Barcode.FORMAT_CODE_128,
            Barcode.FORMAT_CODE_39,
            Barcode.FORMAT_CODE_93,
            Barcode.FORMAT_CODABAR,
            Barcode.FORMAT_EAN_13,
            Barcode.FORMAT_EAN_8,
            Barcode.FORMAT_ITF,
            Barcode.FORMAT_QR_CODE,
            Barcode.FORMAT_UPC_A,
            Barcode.FORMAT_UPC_E,
            Barcode.FORMAT_PDF417,
            Barcode.FORMAT_AZTEC
        )
        .build()

    private val barcodeScanner = BarcodeScanning.getClient(options)

    private var isRunning: Boolean = false

    private val _barcodes = MutableLiveData<List<String>>()
    val barcodes: LiveData<List<String>> get() = _barcodes

    private val _frame = MutableLiveData<Bitmap>()
    val frame: LiveData<Bitmap> get() = _frame

    fun pause() {
        isRunning = false
    }

    fun resume() {
        isRunning = true
    }

    override fun analyze(imageProxy: ImageProxy) {
        try {
            if (!isRunning) return
            processImage(imageProxy)
        } catch (e: Exception) {
            Log.e("ScannerImageAnalyzer", "Error occurred while processing the image", e)
        } finally {
            imageProxy.close()
        }
    }

    @SuppressLint("UnsafeExperimentalUsageError")
    private fun processImage(imageProxy: ImageProxy) {
        val scanReadyImage = scanImageConverter.convert(imageProxy, scannerMeasurements)
            ?: return
        scanReadyImage.bitmap?.let { _frame.postValue(it) }
        detectBarcode(scanReadyImage.inputImage)
    }

    private fun detectBarcode(image: InputImage) {
        val detectionResult = Tasks.await(barcodeScanner.process(image))

        detectionResult
            .mapNotNull { it.displayValue }
            .takeIf { it.isNotEmpty() }
            ?.let { detectedBarcodes ->
                _barcodes.postValue(detectedBarcodes)
                playShutterSound()
            }
    }

    private fun playShutterSound() {
        mediaActionSound.play(MediaActionSound.SHUTTER_CLICK)
    }
}

