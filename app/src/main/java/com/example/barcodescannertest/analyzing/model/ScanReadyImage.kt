package com.example.barcodescannertest.analyzing.model

import android.graphics.Bitmap
import com.google.mlkit.vision.common.InputImage

class ScanReadyImage (val inputImage: InputImage, val bitmap: Bitmap?)